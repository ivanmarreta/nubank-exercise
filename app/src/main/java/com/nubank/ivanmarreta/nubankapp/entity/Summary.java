package com.nubank.ivanmarreta.nubankapp.entity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Ivan on 16/06/2015.
 */
public class Summary {

    private Date dueDate;
    private Date openDate;
    private Date closeDate;
    private BigDecimal pastBalance;
    private BigDecimal totalBalance;
    private BigDecimal interest;
    private BigDecimal totalCumulative;
    private BigDecimal paid;
    private BigDecimal minimumPayment;

    public Summary(){

    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Date getOpenDate() {
        return openDate;
    }

    public void setOpenDate(Date openDate) {
        this.openDate = openDate;
    }

    public Date getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }

    public BigDecimal getPastBalance() {
        return pastBalance;
    }

    public void setPastBalance(BigDecimal pastBalance) {
        this.pastBalance = pastBalance;
    }

    public BigDecimal getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(BigDecimal totalBalance) {
        this.totalBalance = totalBalance;
    }

    public BigDecimal getInterest() {
        return interest;
    }

    public void setInterest(BigDecimal interest) {
        this.interest = interest;
    }

    public BigDecimal getTotalCumulative() {
        return totalCumulative;
    }

    public void setTotalCumulative(BigDecimal totalCumulative) {
        this.totalCumulative = totalCumulative;
    }

    public BigDecimal getPaid() {
        return paid;
    }

    public void setPaid(BigDecimal paid) {
        this.paid = paid;
    }

    public BigDecimal getMinimumPayment() {
        return minimumPayment;
    }

    public void setMinimumPayment(BigDecimal minimumPayment) {
        this.minimumPayment = minimumPayment;
    }
}
