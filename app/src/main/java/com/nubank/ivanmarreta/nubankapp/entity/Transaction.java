package com.nubank.ivanmarreta.nubankapp.entity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Ivan on 16/06/2015.
 */
public class Transaction {

    private Date postDate;
    private BigDecimal amount;
    private String title;
    private int index;
    private int charges;

    public Transaction(){

    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getCharges() {
        return charges;
    }

    public void setCharges(int charges) {
        this.charges = charges;
    }
}
