package com.nubank.ivanmarreta.nubankapp.com.nubank.ivanmarreta.nubankapp.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.nubank.ivanmarreta.nubankapp.R;
import com.nubank.ivanmarreta.nubankapp.entity.Transaction;
import com.nubank.ivanmarreta.nubankapp.util.AndroidSystemUtil;

import java.util.List;

/**
 * Created by Ivan on 22/06/2015.
 */
public class TransactionListViewAdapter extends ArrayAdapter<Transaction> {

    Context context;

    public TransactionListViewAdapter(Context context, List<Transaction> objects) {
        super(context, 0, 0, objects);
        this.context = context;
    }

    @SuppressLint({ "ViewHolder", "InflateParams" })
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Resources resources = context.getResources();
        Transaction transaction = getItem(position);
        convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_transaction_item, null);

        ((TextView)convertView.findViewById(R.id.item_date)).setText(AndroidSystemUtil.diaMes(transaction.getPostDate()));
        String title = transaction.getTitle();
        if(transaction.getCharges() > 1){
            title = title.concat(" " + String.valueOf(transaction.getIndex()+1) + "/" + String.valueOf(transaction.getCharges()));
        }
        ((TextView)convertView.findViewById(R.id.item_title)).setText(title);
        ((TextView)convertView.findViewById(R.id.item_amount)).setText(AndroidSystemUtil.formatarMoeda(transaction.getAmount().toString()));

        return convertView;
    }
}
