package com.nubank.ivanmarreta.nubankapp;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;

import com.nubank.ivanmarreta.nubankapp.com.nubank.ivanmarreta.nubankapp.adapters.ViewPagerFragmentAdapter;
import com.nubank.ivanmarreta.nubankapp.entity.Bill;
import com.nubank.ivanmarreta.nubankapp.entity.Summary;
import com.nubank.ivanmarreta.nubankapp.entity.Transaction;
import com.nubank.ivanmarreta.nubankapp.json.JsonUtil;
import com.nubank.ivanmarreta.nubankapp.util.AndroidSystemUtil;
import com.viewpagerindicator.TitlePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    TitlePageIndicator indicator;
    ViewPager mPager;
    ViewPagerFragmentAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(AndroidSystemUtil.isNetworkAvailable(MainActivity.this)){
            new BillsAsyncTask().execute();
        }
    }


    private class BillsAsyncTask extends AsyncTask<Integer, Void, List<Bill>> {

        @Override
        protected void onPostExecute(List<Bill> list) {
            mAdapter = new ViewPagerFragmentAdapter(getSupportFragmentManager(), list);

            mPager = (ViewPager)findViewById(R.id.pager);
            mPager.setAdapter(mAdapter);

            indicator = (TitlePageIndicator)findViewById(R.id.indicator);
            indicator.setTextSize(30);
            indicator.setViewPager(mPager);
            indicator.setFooterIndicatorStyle(TitlePageIndicator.IndicatorStyle.Triangle);
            indicator.setTextColor(getResources().getColor(R.color.paid_color));
            indicator.setSelectedColor(getResources().getColor(R.color.paid_color));
            indicator.setFooterColor(getResources().getColor(R.color.paid_color));

            indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageSelected(int position) {
                    if (mPager.getCurrentItem() == 0) {
                        indicator.setSelectedColor(getResources().getColor(R.color.paid_color));
                        indicator.setFooterColor(getResources().getColor(R.color.paid_color));
                    }
                    if (mPager.getCurrentItem() == 1) {
                        indicator.setSelectedColor(getResources().getColor(R.color.closed_color));
                        indicator.setFooterColor(getResources().getColor(R.color.closed_color));
                    }
                    if (mPager.getCurrentItem() == 2) {
                        indicator.setSelectedColor(getResources().getColor(R.color.opened_color));
                        indicator.setFooterColor(getResources().getColor(R.color.opened_color));
                    }
                    if (mPager.getCurrentItem() == 3) {
                        indicator.setSelectedColor(getResources().getColor(R.color.future_color));
                        indicator.setFooterColor(getResources().getColor(R.color.future_color));
                    }
                }

                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });
        }

        @Override
        protected List<Bill> doInBackground(Integer... params) {
            return getBills();
        }

        private List<Bill> getBills() {

            String result = JsonUtil.get("https://s3-sa-east-1.amazonaws.com/mobile-challenge/bill/bill_new.json");
            List<Bill> listBill = new ArrayList<Bill>();
            try {
                JSONArray jsonArray = new JSONArray(result);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject billJson = new JSONObject(jsonArray.getString(i)).getJSONObject("bill");
                    Bill bill = new Bill();
                    bill.setState(billJson.getString("state"));
                    try {
                        bill.setId(billJson.getString("id"));
                    }catch (Exception e){

                    }

                    JSONObject summmaryJson = billJson.getJSONObject("summary");

                    Summary summary = new Summary();
                    summary.setDueDate(formatSimpleStringDate(summmaryJson.getString("due_date")));
                    summary.setCloseDate(formatSimpleStringDate(summmaryJson.getString("close_date")));
                    summary.setPastBalance(new BigDecimal(summmaryJson.getString("past_balance")));
                    summary.setTotalBalance(new BigDecimal(summmaryJson.getString("total_balance")));
                    summary.setInterest(new BigDecimal(summmaryJson.getString("interest")));
                    summary.setTotalCumulative(new BigDecimal(summmaryJson.getString("total_cumulative")));
                    try{
                        summary.setPaid(new BigDecimal(summmaryJson.getString("paid")));
                    }catch (Exception e){
                    }
                    summary.setMinimumPayment(new BigDecimal(summmaryJson.getString("minimum_payment")));
                    summary.setOpenDate(formatSimpleStringDate(summmaryJson.getString("open_date")));

                    bill.setSummary(summary);

                    List<Transaction> listTransaction = new ArrayList<Transaction>();
                    JSONArray transactionArrayJson = billJson.getJSONArray("line_items");
                    for (int t = 0; t < transactionArrayJson.length(); t++) {
                        JSONObject transactionJson = new JSONObject(transactionArrayJson.getString(t));

                        Transaction transaction = new Transaction();
                        transaction.setPostDate(formatSimpleStringDate(transactionJson.getString("post_date")));
                        transaction.setAmount(new BigDecimal(transactionJson.getString("amount")));
                        transaction.setTitle(transactionJson.getString("title"));
                        try {
                            transaction.setIndex(transactionJson.getInt("index"));
                            transaction.setCharges(transactionJson.getInt("charges"));
                        }catch (Exception e){
                        }

                        listTransaction.add(transaction);
                    }

                    bill.setListTransaction(listTransaction);
                    listBill.add(bill);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (result == null) {
                return null;
            }
            return listBill;
        }

        private Date formatSimpleStringDate(String dateString){
            try {
                return new SimpleDateFormat("yyyy-MM-dd").parse(dateString);
            } catch (Exception e) {
                return null;
            }
        }
    }
}
