package com.nubank.ivanmarreta.nubankapp.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Ivan on 16/06/2015.
 */
public class Bill implements Serializable{

    private static final long serialVersionUID = 1L;

    private String state;
    private String id;
    private Summary summary;
    private List<Transaction> listTransaction;

    public Bill(){
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Summary getSummary() {
        return summary;
    }

    public void setSummary(Summary summary) {
        this.summary = summary;
    }

    public List<Transaction> getListTransaction() {
        return listTransaction;
    }

    public void setListTransaction(List<Transaction> listTransaction) {
        this.listTransaction = listTransaction;
    }
}
