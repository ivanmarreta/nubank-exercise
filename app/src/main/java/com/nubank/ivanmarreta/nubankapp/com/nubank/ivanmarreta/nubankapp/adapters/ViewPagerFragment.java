package com.nubank.ivanmarreta.nubankapp.com.nubank.ivanmarreta.nubankapp.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.nubank.ivanmarreta.nubankapp.R;
import com.nubank.ivanmarreta.nubankapp.entity.Bill;
import com.nubank.ivanmarreta.nubankapp.util.AndroidSystemUtil;

import java.math.BigDecimal;

/**
 * Created by Ivan on 16/06/2015.
 */
public class ViewPagerFragment extends Fragment {
    private static final String KEY_CONTENT = "ViewPagerFragment:Content";

    private int position;
    private Bill bill;

    public static ViewPagerFragment newInstance(int position, Bill bill) {
        ViewPagerFragment fragment = new ViewPagerFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        args.putSerializable("bill", bill);
        fragment.setArguments(args);
        return fragment;
    }

    private String mContent = "???";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt("position", 0);
        bill = (Bill) getArguments().getSerializable("bill");

        if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT)) {
            mContent = savedInstanceState.getString(KEY_CONTENT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.bill_page_view, container, false);
        FrameLayout topFrame = (FrameLayout)view.findViewById(R.id.top_frame);

        FrameLayout paid_frame = (FrameLayout)view.findViewById(R.id.paid_frame);
        paid_frame.setVisibility(View.GONE);

        LinearLayout closed_info = (LinearLayout)view.findViewById(R.id.closed_info);
        closed_info.setVisibility(View.GONE);

        LinearLayout button_boleto = (LinearLayout)view.findViewById(R.id.button_boleto);
        button_boleto.setVisibility(View.GONE);

        TextView summary_value = (TextView)view.findViewById(R.id.summary_value);
        summary_value.setText(AndroidSystemUtil.formatarMoeda(bill.getSummary().getTotalBalance().toString()));

        TextView vencimento = (TextView)view.findViewById(R.id.vencimento);
        vencimento.setText(getResources().getString(R.string.vencimento).concat(" " + AndroidSystemUtil.diaMes(bill.getSummary().getDueDate())));

        TextView paid_value = (TextView)view.findViewById(R.id.paid_value);
        if(bill.getSummary().getPaid() != null){
            paid_value.setText(AndroidSystemUtil.formatarMoeda(bill.getSummary().getPaid().toString()));
        }

        TextView until_date = (TextView)view.findViewById(R.id.until_date);
        until_date.setText("DE ".concat(AndroidSystemUtil.diaMes(bill.getSummary().getOpenDate())).concat(" ATE ").concat(AndroidSystemUtil.diaMes(bill.getSummary().getCloseDate())));

        TransactionListViewAdapter transactionListViewAdapter = new TransactionListViewAdapter(getActivity(), bill.getListTransaction());
        ListView listview = (ListView)view.findViewById(R.id.listview);
        listview.setAdapter(transactionListViewAdapter);

        if(bill.getState().equals("overdue")){
            paid_frame.setVisibility(View.VISIBLE);

            topFrame.setBackgroundColor(getResources().getColor(R.color.paid_color));
        }

        if (bill.getState().equals("closed")) {
            closed_info.setVisibility(View.VISIBLE);
            button_boleto.setVisibility(View.VISIBLE);

            if(maiorQueZero(bill.getSummary().getTotalCumulative())){
                TextView total_cumulative = (TextView)view.findViewById(R.id.total_cumulative);
                total_cumulative.setText(AndroidSystemUtil.formatarMoeda(bill.getSummary().getTotalCumulative().toString()));
            }

            TextView tv_past_balance = (TextView)view.findViewById(R.id.tv_past_balance);
            TextView past_balance = (TextView)view.findViewById(R.id.past_balance);
            past_balance.setText(AndroidSystemUtil.formatarMoeda(bill.getSummary().getPastBalance().toString()));
            if(!maiorQueZero(bill.getSummary().getTotalCumulative())){
                tv_past_balance.setText(getResources().getString(R.string.valores_pre_pagos));
            }

            if(maiorQueZero(bill.getSummary().getInterest())){
                TextView juros = (TextView)view.findViewById(R.id.juros);
                juros.setText(AndroidSystemUtil.formatarMoeda(bill.getSummary().getInterest().toString()));
            }

            topFrame.setBackgroundColor(getResources().getColor(R.color.closed_color));
        }

        if (bill.getState().equals("open")) {
            button_boleto.setVisibility(View.VISIBLE);

            Button bt_boleto = (Button)view.findViewById(R.id.bt_boleto);
            bt_boleto.setBackground(getResources().getDrawable(R.drawable.button_blue_layout));
            bt_boleto.setTextColor(getResources().getColor(R.color.opened_color));

            topFrame.setBackgroundColor(getResources().getColor(R.color.opened_color));
        }

        if (bill.getState().equals("future")) {
            topFrame.setBackgroundColor(getResources().getColor(R.color.future_color));
        }

        return view;
    }

    private boolean maiorQueZero(BigDecimal value){
        return value.compareTo(BigDecimal.ZERO) == 1;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_CONTENT, mContent);
    }
}