package com.nubank.ivanmarreta.nubankapp.util;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.nubank.ivanmarreta.nubankapp.R;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Currency;
import java.util.Date;

/**
 * Created by Ivan on 22/06/2015.
 */
public class AndroidSystemUtil {

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager conectivtyManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (conectivtyManager.getActiveNetworkInfo() != null  && conectivtyManager.getActiveNetworkInfo().isAvailable()
                && conectivtyManager.getActiveNetworkInfo().isConnected()) {
            return true;
        }

        showDescription(context);
        return false;
    }

    private static void showDescription(Context context){
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.without_network_dialog);

        TextView buttonConfirm = (TextView)dialog.findViewById(R.id.dialog_custom_confirm);

        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public static String formatarMoeda(String texto){
        NumberFormat formatoMoeda = NumberFormat.getCurrencyInstance();
        formatoMoeda.setCurrency(Currency.getInstance("BRL"));
        try {
            String formatado = formatoMoeda.format(Double.valueOf(texto)).replace(" ", ".").replace(" ", ".").replace(")", "").replace("R$", "R$ ").replace("(R$", "R$ -").trim();
            return formatado.endsWith("0") ? formatado : formatado.substring(0, formatado.length()-1);
        } catch (Exception e) {
            return texto;
        }
    }

    public static String diaMes(Date date){
        DateFormat diaFmt = new SimpleDateFormat("d");
        DateFormat mesFmt = new SimpleDateFormat("MMMM");
        String dia = diaFmt.format(date).toUpperCase();
        String mes = mesFmt.format(date).toUpperCase().substring(0, 3);

        return dia + " " + mes;
    }
}
