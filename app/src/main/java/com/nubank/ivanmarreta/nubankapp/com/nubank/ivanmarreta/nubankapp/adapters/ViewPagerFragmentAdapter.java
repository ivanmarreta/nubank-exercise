package com.nubank.ivanmarreta.nubankapp.com.nubank.ivanmarreta.nubankapp.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.nubank.ivanmarreta.nubankapp.entity.Bill;

import java.util.List;

/**
 * Created by Ivan on 16/06/2015.
 */
public class ViewPagerFragmentAdapter extends FragmentPagerAdapter {
    protected static final String[] CONTENT = new String[] { "MAI", "JUN", "JUL", "AGO", };

    private int mCount = CONTENT.length;
    private List<Bill> list;

    public ViewPagerFragmentAdapter(FragmentManager fm, List<Bill> list) {
        super(fm);
        this.list = list;
    }

    @Override
    public Fragment getItem(int position) {
        return ViewPagerFragment.newInstance(position, list.get(position));
    }

    @Override
    public int getCount() {
        return mCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return ViewPagerFragmentAdapter.CONTENT[position % CONTENT.length];
    }

    public void setCount(int count) {
        if (count > 0 && count <= 10) {
            mCount = count;
            notifyDataSetChanged();
        }
    }
}

